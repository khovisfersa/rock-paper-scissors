

public class Main {

	public static void main(String[] args) {
		System.out.println("Let the games begin!");
		player B = new player();
		int i; 
		for(i=0;i<=99;i++) {
			String hand = null;
			hand = B.play();
			switch(hand) {
				case "rock":
					B.lose();
					break;
				case "paper":
					B.tie();
					break;
				case "scissors":
					B.win();
					break;
			}
		}

		System.out.println("player B won "+B.getWins()+" times");
		System.out.println("player A won "+B.getDefeats()+" times");
		System.out.println("the players tied "+B.getTies()+" times");
	}

}
