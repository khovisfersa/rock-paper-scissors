import java.util.Random;

public class player {
	
	private int wins, defeats, ties;
	//constructor
	
	public player() {
		this.setDefeats(0);
		this.setTies(0);
		this.setWins(0);
	}
	
	// getters and setters

	public int getDefeats() {
		return defeats;
	}

	public void setDefeats(int defeats) {
		this.defeats = defeats;
	}

	public int getTies() {
		return ties;
	}

	public void setTies(int ties) {
		this.ties = ties;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}
	
	//methods 
	
	public String play() {
		Random rand = new Random();
		int gen = rand.nextInt(3)+1;
		String hand = null;
		switch(gen) {
			case 1:
				hand = "rock";
				break;
			case 2:
				hand = "paper";
				break;
			case 3:
				hand = "scissors";
				break;
		}
		return hand;
		
		
	}

	public void win() {
		int vic = this.getWins();
		this.setWins(vic+1);
	}
	
	public void lose() {
		int loss = this.getDefeats();
		this.setDefeats(loss+1);
	}
	
	public void tie() {
		int tie = this.getTies();
		this.setTies(tie+1);
	}
}
